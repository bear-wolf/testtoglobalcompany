FROM node:18-alpine
RUN echo 'Run api part'
WORKDIR /usr/src/app

ARG NODE_ENV
ENV NODE_ENV $NODE_ENV

COPY package*.json /usr/src/app/
RUN npm install
COPY . /usr/src/app
EXPOSE 4000

CMD [ "npm", "run", "start" ]
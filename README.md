# TestToGLOBALCompany

# Set environment

1.) Up db mongo with docker
> npm run start:mongo 

2.) Prepare to migration
Take IP address docker image our mongo service with property **IPAddress**:
> docker inspect test-mongo

3.) Set above IP address to ./migrations/migrate-mongo-config.js in **URL** property like this
> nano ./migrations/migrate-mongo-config.js

Change url property like this "mongodb://172.17.0.2:27017"

* Use database like: **global_co** by default

4.) Run migration like:
> cd /migrations
> npm run up

# Run process

- Run API part
> npm run start

# Swagger
- http://localhost:4000/doc

# Examples:
- Query, sort  
    createdAt = desc | ask  
    updatedAt = desc | ask  
    status = 0 | 1 | 2  
    limit - any number  

> http://localhost:4000/v1/users/list?createdAt=desc
> http://localhost:4000/v1/tasks/list?createdAt=desc
> http://localhost:4000/v1/projects/list?createdAt=desc
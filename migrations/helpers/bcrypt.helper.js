require('dotenv').config();
const bcrypt = require('bcrypt');

const hashPassword = async (password) => {
    const saltRounds = 10;
    const secret = process.env.SECRET;

    return await new Promise((resolve, reject) => {
        bcrypt.hash(`${password}${secret}`, saltRounds, (err, hash) => {
            err && reject(err) || resolve(hash)
        });
    })
}

const comparePassword = async (inputPassword, storePassword) => {
   return await new Promise((resolve, reject) => {
       bcrypt.compare(inputPassword, storePassword, (err, result) => {
           err && reject(err) || resolve({})
       });
   })
}

module.exports = {
    hashPassword,
    comparePassword
}
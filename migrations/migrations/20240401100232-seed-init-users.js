const moment = require("moment");

module.exports = {
  async up(db, client) {
    await db.collection('users').insertMany([{
      first_name: 'user1',
      last_name: 'user1',
      username: 'user1',
      is_delete: false,
      email: 'user1@gmail.com',
      password: '12345',
      picture: null,
      test: true,
      authorization_at: null,
      created_at: moment().unix(),
      updated_at: null
    },{
      first_name: 'user2',
      last_name: 'user2',
      username: 'user2',
      is_delete: false,
      email: 'user2@gmail.com',
      password: '12345',
      picture: null,
      test: true,
      authorization_at: null,
      created_at: moment().unix(),
      updated_at: null
    },{
      first_name: 'user3',
      last_name: 'user3',
      username: 'user3',
      is_delete: false,
      email: 'user3@gmail.com',
      password: '12345',
      picture: null,
      test: true,
      authorization_at: null,
      created_at: moment().unix(),
      updated_at: null
    }]);
  },

  async down(db, client) {
  }
};

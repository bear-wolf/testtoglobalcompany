module.exports = {
    async up(db, client) {
        await db.createCollection('tasks')
    },

    async down(db, client) {
        await db.collection("tasks").drop();
    }
};
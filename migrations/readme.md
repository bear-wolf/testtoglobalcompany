# migrate-mongo

More information: https://www.npmjs.com/package/migrate-mongo




$ migrate-mongo init -m esm

$ migrate-mongo status
┌─────────────────────────────────────────┬────────────┐
│ Filename                                │ Applied At │
├─────────────────────────────────────────┼────────────┤
│ 20160608155948-blacklist_the_beatles.js │ PENDING    │
└─────────────────────────────────────────┴────────────┘

$ npx migrate-mongo up
MIGRATED UP: 20160608155948-blacklist_the_beatles.js

$ npx migrate-mongo down
MIGRATED DOWN: 20160608155948-blacklist_the_beatles.js


$ migrate-mongo status -f '~/configs/albums-migrations.js'
┌─────────────────────────────────────────┬────────────┐
│ Filename                                │ Applied At │
├─────────────────────────────────────────┼────────────┤
│ 20160608155948-blacklist_the_beatles.js │ PENDING    │
└─────────────────────────────────────────┴────────────┘
import {MiddlewareConsumer, Module, NestModule} from '@nestjs/common';
import {AppController} from './app.controller';
import {AppService} from './app.service';
import {SharedModule} from './modules/shared/shared.module';
import {UserModule} from './modules/user/user.module';
import {AuthModule} from './modules/auth/auth.module';
import {TestModule} from "./modules/test/test.module";
import {LoggerMiddleware} from "./middlewares/logger.middleware";
import {TaskModule} from "./modules/task/task.module";
import {ProjectModule} from "./modules/project/project.module";

@Module({
    imports: [
        SharedModule,
        TestModule,
        UserModule,
        TaskModule,
        ProjectModule,
        AuthModule
    ],
    controllers: [AppController],
    providers: [
        LoggerMiddleware,
        AppService,
    ],
})
export class AppModule implements NestModule {
    configure(consumer: MiddlewareConsumer) {
        consumer
            .apply(LoggerMiddleware)
            .forRoutes('sign-in');
    }
}

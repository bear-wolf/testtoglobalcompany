import {Prop} from "@nestjs/mongoose";
import {SchemaTypes} from "mongoose";

export class isIDInParamsDto {
    @Prop({type: SchemaTypes.ObjectId})
    _id: string;
}


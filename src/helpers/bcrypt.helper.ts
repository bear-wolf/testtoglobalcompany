import 'dotenv/config';
import bcrypt from 'bcrypt'

export const hashPassword = async (password: string): Promise<void> => {
    const saltRounds = 10;
    const secret = process.env.SECRET;
    return await new Promise((resolve: any, reject: any) => {
        bcrypt.hash(`${password}${secret}`, saltRounds, (err: any, hash: string) => {
            err && reject(err) || resolve(hash)
        });
    })
}

export const comparePassword = async (inputPassword: string, storePassword: string) => {
    return await new Promise((resolve: any, reject: any) => {
        bcrypt.compare(inputPassword, storePassword, (err, result) => {
            err && reject(err) || resolve({})
        });
    })
}

export default {
    hashPassword,
    comparePassword
}
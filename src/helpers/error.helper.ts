import {NextFunction, Request, Response} from "express";
import { HttpException, HttpStatus } from '@nestjs/common';
import { Logger } from '@nestjs/common';

const getCodeError = () => {
    return {
        400: "Bad Request",
        401: "Not Found",
        403: "Not Found",
        404: "Not Found",
        500: "Internal Server Error",
    }
}

export const parseError = (error: any, req: Request, res: Response, next: NextFunction) => {
    // const statusCode = function ()
    switch (error.status) {
        case 400: {
            break;
        }
        case 401: {
            break;
        }
        case 403: {
            break;
        }
        case 500: {
            res.status(500).send({ error: 'Something failed!' })
            break;
        }
    }
    next(error)
}

export const makeHttpException = (message: string, code: HttpStatus) => {
    return {
        error: new HttpException(message, code)
    }
}

export const parseSQLError = (error: any) => {
    // Logger.error('ERROR', {
    //     date: new Date().toISOString(),
    //     ...error
    // })
    switch (error.name) {
        case 'ConnectionError': {
            throw new HttpException({
                status: HttpStatus.BAD_REQUEST,
                error: error.meta.meta.name,
                message: 'Please check the connection to ElasticSearch',
            }, HttpStatus.BAD_REQUEST, {
                cause: error
            });
        }
        case 'SequelizeUniqueConstraintError':
            throw new HttpException({
                status: HttpStatus.BAD_REQUEST,
                error: error.errors.length && error.errors[0].message,
            }, HttpStatus.BAD_REQUEST, {
                cause: error
            });
        case 'ProductNotSupportedError':
            throw new HttpException({
                status: HttpStatus.BAD_REQUEST,
                error: 'As you are using 7.10 version of ElasticSearch and elastic client is 7.12 hence it is throwing this exception.'
                       + 'Please remove the ElasticSearch client 7.12 and install ElasticSearch client 7.10 version which will resolved your issue.',
            }, HttpStatus.BAD_REQUEST, {
                cause: error
            });
    }
    return error
}

export const parseJWTError = (error: any): Promise<any> => {
    switch (error.status) {
        case 'refreshIsExpired': {
            throw new HttpException({
                status: HttpStatus.UNAUTHORIZED,
                message: 'RefreshToken is expired',
            }, HttpStatus.BAD_REQUEST, {
                cause: error
            });
        }
    }
    return Promise.reject()
}

export default {
    parseSQLError,
    parseJWTError,
    parseError
}
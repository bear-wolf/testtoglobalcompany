import {Body, Controller, HttpException, HttpStatus, Inject, Logger, Post, Res} from '@nestjs/common';
import {AuthService} from './auth.service';
import {WINSTON_MODULE_PROVIDER} from 'nest-winston';
import {SignUpUserDto} from './dto/sign-up-user.dto';
import {SignInUserDto} from './dto/sign-in-user.dto';
import {Response} from 'express';
import {JWTService} from '../shared/jwt.service';
import {RefreshTokenUserDto} from "./dto/refresh-token-user.dto";
import errorHelper from "../../helpers/error.helper";
import {ApiOperation, ApiResponse} from "@nestjs/swagger";

@Controller('auth')
export class AuthController {
    constructor(
        private readonly JWTService: JWTService,
        @Inject(WINSTON_MODULE_PROVIDER) public readonly logger: Logger,
        private readonly authService: AuthService) {
    }

    @ApiOperation({summary: 'Authenticate with registration user before'})
    @ApiResponse({status: 200, description: 'Successfully'})
    @Post('sign-in')
    async signIn(@Body() body: SignInUserDto): Promise<any> {
        const user: any = await this.authService.signIn(body);
        if (!user)
            return new HttpException('User not found', HttpStatus.NOT_FOUND)

        const {accessToken, refreshToken} = await this.JWTService.getJWTTokens(user._id, {
            _id: user._id,
            email: user.email
        });

        user.authorizationAt = Date.now();
        await user.save();

        return {
            ...user.toJSON(),
            accessToken,
            refreshToken
        }
    }

    @ApiOperation({summary: 'Registration user'})
    @ApiResponse({status: 200, description: 'Successfully'})
    @Post('sign-up')
    async signUp(@Body() body: SignUpUserDto, @Res() res: Response): Promise<any> {
        const {data, error} = await this.authService.signUp(body);

        return error
            ? res.status(error.status).json(error)
            : res.status(200).json(data);
    }


    @ApiOperation({summary: 'Get re-new access token with exist refresh token'})
    @ApiResponse({status: 200, description: 'Successfully'})
    @Post('renew-refresh-token')
    async renewRefreshToken(@Body() body: RefreshTokenUserDto, @Res() responce): Promise<any> {
        const {refreshToken} = body;
        const data = this.JWTService.refreshTokenIsExpire(refreshToken);

        if (data.error)
            return errorHelper.parseJWTError({
                status: 'refreshIsExpired'
            });

        const cryptData: { id: string, email: string } = this.JWTService.decodeData(refreshToken);
        let tokens;

        try {
            tokens = await this.JWTService.getJWTTokens(cryptData.id, {
                id: cryptData.id,
                email: cryptData.email
            });
        } catch (error) {
            return responce.status(HttpStatus.BAD_REQUEST).json({})
        }

        return responce.status(HttpStatus.OK).json({
            refreshToken: tokens.refreshToken
        });
    }
}

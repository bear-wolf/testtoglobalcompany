import {HttpException, HttpStatus, Injectable} from '@nestjs/common';
import 'dotenv/config';
import {User, UserDocument} from '../shared/schemas/users.entity';
import errorHelper, {makeHttpException} from '../../helpers/error.helper';
import {InjectModel} from "@nestjs/mongoose";
import {Model} from 'mongoose';
import * as mongoose from "mongoose";
import bcryptHelper from "../../helpers/bcrypt.helper";
import * as moment from "moment";

@Injectable()
export class AuthService {
    constructor(
        @InjectModel(User.name) private user: Model<User>) {}

    async isDuplicate(email: string, password: string): Promise<any> {
        const user = await this.user.findOne({
            email,
            password
        });
        return !!user;
    }

    async signUp(data: any): Promise<any> {
        const { email, password } = data;
        let user: any;

        try {
            if (await this.isDuplicate(email, password))
                return makeHttpException("User already exists", HttpStatus.BAD_REQUEST)

            data._id = new mongoose.Types.ObjectId()
            data.createdAt = moment().unix()
            //TODO: It's not working in my version. Maybe bcrypt library is break
            //data.password = await bcryptHelper.hashPassword(data.password)
            user = await this.user.create(data);
        } catch (error) {
            console.log(error);
            return errorHelper.parseSQLError(error);
        }

        return {
            data: user
        };
    }

    async signIn(data: any): Promise<any> {
        let { email, password } = data;
        //TODO: hash disable because there is a problem with the library
        //password = await bcryptHelper.hashPassword(data.password)

        const user: UserDocument = await this.user.findOne({
            email,
            password
        });
        return user;
    }
}

import {IsString} from 'class-validator';
import {ApiProperty} from "@nestjs/swagger";

export class RefreshTokenUserDto {
    @ApiProperty({description: 'Refresh token'})
    @IsString({
        always: true,
        message: 'RefreshToken is required'
    })
    refreshToken: string;
}
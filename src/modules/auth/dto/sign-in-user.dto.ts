import {IsEmail, IsNotEmpty} from 'class-validator';
import {ApiProperty} from "@nestjs/swagger";

export class SignInUserDto {
    @ApiProperty({description: 'Email'})
    @IsEmail()
    email: string;

    @ApiProperty({description: 'Password'})
    @IsNotEmpty()
    password: string;
}
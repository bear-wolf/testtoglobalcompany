import {IsEmail, IsNotEmpty, IsString} from 'class-validator';
import {ApiProperty} from "@nestjs/swagger";

export class SignUpUserDto {
    @ApiProperty({description: 'First name'})
    @IsString()
    firstName: string;

    @ApiProperty({description: 'Last name'})
    @IsString()
    lastName: string;

    @ApiProperty({description: 'Username'})
    @IsString()
    username: string;

    @ApiProperty({description: 'Email'})
    @IsEmail()
    email: string;

    @ApiProperty({description: 'Password'})
    @IsNotEmpty()
    password: string;
}
import {IsNumber, IsOptional, IsString} from 'class-validator';
import {Exclude} from 'class-transformer';
import {Prop} from "@nestjs/mongoose";
import {SchemaTypes} from "mongoose";
import {ApiProperty, ApiPropertyOptional} from "@nestjs/swagger";

export class GetListQueryDto {
  @ApiPropertyOptional({ description: 'Status 0:new, 1:in_progress 2:done' })
  @IsOptional()
  @Prop()
  status: number

  @ApiPropertyOptional({ description: 'Limit' })
  @IsOptional()
  limit: number

  @ApiPropertyOptional({ description: 'Created date' })
  @IsOptional()
  @Prop()
  createdAt: 'desc' | 'asc'

  @ApiPropertyOptional({ description: 'Updated date' })
  @IsOptional()
  @Prop()
  updatedAt: 'desc' | 'asc'
}
import {IsNumber, IsOptional, IsString} from 'class-validator';
import {Exclude} from 'class-transformer';
import {Prop} from "@nestjs/mongoose";
import {SchemaTypes} from "mongoose";
import {ApiProperty, ApiPropertyOptional} from "@nestjs/swagger";

export class ProjectDto {
    @Prop({ type: SchemaTypes.ObjectId})
    _id: string

    @ApiProperty({ description: 'Project name' })
    @Prop()
    name: string;

    @ApiProperty({ description: 'Description' })
    @Prop()
    description: string;

    @ApiProperty({ description: 'UserID' })
    @Prop({
        required: true,
        field: 'user_id'
    })
    userID: string;

    @ApiPropertyOptional({ description: 'Picture like base64' })
    @Prop()
    picture: string

    @ApiPropertyOptional({ description: 'Created date' })
    @Prop()
    createdAt: Date;

    @ApiPropertyOptional({ description: 'Updated date' })
    @IsOptional()
    @Prop()
    updatedAt: Date;
}
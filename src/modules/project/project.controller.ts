import {
    Body,
    Controller,
    Delete,
    Get,
    HttpException, HttpStatus,
    NotFoundException,
    Param,
    Post,
    Put, Query,
    Req,
    Res
} from '@nestjs/common';
import {ProjectService} from "./project.service";
import {isIDInParamsDto} from "../../dtos/isID.dto";
import {Request, Response} from "express";
import {ProjectDto} from "./dto/project.dto";
import {GetListQueryDto} from "./dto/get-list-query.dto";
import {ApiOperation, ApiResponse} from "@nestjs/swagger";

//@UseGuards(AuthGuard)
@Controller('projects')
export class ProjectController {
    constructor(
        private readonly projectService: ProjectService) {
    }

    @ApiOperation({ summary: 'Get list of the projects' })
    @ApiResponse({ status: 200, description: 'Successfully' })
    @Get('list')
    async getList(@Query() query: GetListQueryDto): Promise<any[]> {
        return await this.projectService.getList(query);
    }

    @ApiOperation({ summary: 'Create the project' })
    @ApiResponse({ status: 200, description: 'Successfully' })
    @ApiResponse({ status: HttpStatus.NOT_FOUND, description: 'Not found' })
    @Post()
    async create(@Body() schema: ProjectDto, @Req() req: Request, @Res() res: Response): Promise<any> {
        let project;

        try {
            project = await this.projectService.create(schema);
        } catch (error) {
            throw new HttpException('Something wrong', HttpStatus.INTERNAL_SERVER_ERROR)
        }

        res.status(200).json(project);
    }

    @ApiOperation({ summary: 'Get the project by ID' })
    @ApiResponse({ status: 200, description: 'Successfully' })
    @ApiResponse({ status: HttpStatus.NOT_FOUND, description: 'Not found' })
    @Post(':_id')
    async getByID(@Param() body: isIDInParamsDto, @Res() res: Response): Promise<any> {
        const {_id} = body;
        const project = await this.projectService.getByID(_id);

        if (!project) throw new NotFoundException(`Project not found`);

        res.status(200).json(project || {});
    }

    @ApiOperation({ summary: 'Update the project by ID' })
    @ApiResponse({ status: 200, description: 'Successfully' })
    @ApiResponse({ status: HttpStatus.NOT_FOUND, description: 'Not found' })
    @Put(':_id')
    async update(@Param() body: isIDInParamsDto, @Body() schema: ProjectDto, @Req() req: Request, @Res() res: Response): Promise<any> {
        const {_id} = body;
        let project;

        try {
            project = await this.projectService.update(_id, schema);
        } catch (error) {
            throw new HttpException('Something wrong', HttpStatus.INTERNAL_SERVER_ERROR)
        }

        res.status(200).json(project);
    }

    @ApiOperation({ summary: 'Remove the project by ID' })
    @ApiResponse({ status: 200, description: 'Successfully' })
    @Delete(':_id')
    async remove(@Param() body: isIDInParamsDto, @Res() res: Response): Promise<any> {
        const {_id} = body;
        await this.projectService.remove(_id);
        res.status(200).json({});
    }
}

import {Module} from '@nestjs/common';
import {ProjectController} from "./project.controller";
import {ProjectService} from "./project.service";
import {SharedModule} from "../shared/shared.module";

@Module({
    imports: [
        SharedModule
    ],
    controllers: [
        ProjectController
    ],
    exports: [
        ProjectService
    ],
    providers: [
        ProjectService
    ]
})
export class ProjectModule {
}

import {HttpException, HttpStatus, Injectable, NotFoundException} from '@nestjs/common';
import 'dotenv/config';
import {InjectModel} from "@nestjs/mongoose";
import mongoose, {Model} from 'mongoose';
import {Project, ProjectDocument, ProjectStatus} from "../shared/schemas/projects.entity";
import * as moment from 'moment';
import {GetListQueryDto} from "./dto/get-list-query.dto";

@Injectable()
export class ProjectService {
    constructor(
        @InjectModel(Project.name) private project: Model<Project>) {
    }

    async getList(query?: GetListQueryDto): Promise<any[]> {
        const {status, limit, createdAt, updatedAt} = query;
        return await this.project.find({
            ...(status && {status})
        }, {}, {
            ...(limit && {limit})
        })
            .sort({
                ...(createdAt && {createdAt}),
                ...(updatedAt && {updatedAt})
            })
    }

    async create(body: any): Promise<any> {
        const object = {
            _id: new mongoose.Types.ObjectId(),
            status: ProjectStatus.NEW,
            ...(body.name && {name: body.name}),
            ...(body.description && {description: body.description}),
            ...(body.userID && {userID: body.userID}),
            ...(body.projectID && {projectID: body.projectID}),
            createdAt:  moment().unix()
        }
        return await this.project.create(object);
    }

    async getByID(id: string): Promise<ProjectDocument> {
        return await this.project.findById(id)
    }

    async update(_id: string, body: any): Promise<any> {
        const project: ProjectDocument = await this.project.findById(_id);

        if (!project) throw new NotFoundException(`Project not found`);

        body.name && (project.name = body.name);
        body.description && (project.description = body.description);
        body.picture && (project.picture = body.picture);

        if (body.status) {
            if (![ProjectStatus.NEW, ProjectStatus.In_Progress, ProjectStatus.Done].includes(body.status)) {
                throw new HttpException('Status param is not correct', HttpStatus.BAD_REQUEST)
            }
            project.status = body.status
        }
        project.updatedAt = moment().unix();
        await project.save();

        return project
    }

    async remove(_id: string): Promise<any> {
        await this.project.deleteOne({_id});
        return {};
    }
}

import {CanActivate, ExecutionContext, Injectable} from "@nestjs/common";
import {Reflector} from "@nestjs/core";
import {JWTService} from "../jwt.service";

@Injectable()
export class AuthGuard implements CanActivate
{
    constructor(
        private JWTService: JWTService,
        private reflector: Reflector) { }

    async canActivate(
        ctx: ExecutionContext
    ): Promise<boolean> {
        let request = ctx
            .switchToHttp()
            .getRequest();
        const {authorization} = request.headers

        try {
            if (!authorization || this.JWTService.accessTokenIsExpire(authorization)) return false
        } catch (error) {
            return false
        }

        request.userdata = this.JWTService.decodeData(authorization);

        return request.userdata && true || false
    }
}
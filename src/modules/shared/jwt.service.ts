import {Inject, Injectable} from '@nestjs/common';
import {JwtService} from '@nestjs/jwt';
import * as moment from 'moment';
import 'dotenv/config';
import {makeHttpException} from "../../helpers/error.helper";
import {CACHE_MANAGER} from "@nestjs/cache-manager";
import {CacheStore} from "@nestjs/common/cache";

@Injectable()
export class JWTService {

    constructor(
        private jwtService: JwtService,
        @Inject(CACHE_MANAGER) private cacheManager: CacheStore
    ) {
    }

    async getJWTTokens(ID: string, userData: object): Promise<any> {
        const isCash = await this.cacheManager.get(ID.toString());

        if (isCash) return isCash;

        const data = {
            accessToken: await this.jwtService.signAsync({
                ...userData,
                expired: moment().add(process.env.ACCESS_TOKEN_EXPIRED_MINUTES, 'minutes').unix(),
            }, {
                privateKey: process.env.PRIVATE_KEY
            }),
            refreshToken: await this.jwtService.signAsync({
                expired: moment().add(process.env.REFRESH_TOKEN_EXPIRED_MINUTES, 'minutes').unix()
            }, {
                privateKey: process.env.PRIVATE_KEY
            }),
        };
        await this.cacheManager.set(ID.toString(), data, {
          ttl: 60 * 60 * 24, // 1D
        });
        return data;
    }

    accessTokenIsExpire(accessToken: string) {
        const data: any = this.jwtService.decode(accessToken, {});
        return data.expired < moment().unix();
    }

    refreshTokenIsExpire(refreshToken: string): any {
        const data: any = this.jwtService.decode(refreshToken, {});

        return data
            ? {data: data.expired < moment().unix()}
            : makeHttpException('Token is broken', 403)
    }

    decodeData(token: string): any {
        return this.jwtService.decode(token, {});
    }
}

import {User, UserSchema, UserTableName} from './users.entity';
import {Task, TaskSchema, TaskTableName} from "./tasks.entity";
import {Project, ProjectSchema, ProjectTableName} from "./projects.entity";

export const EntityProviders = [
    {
        provide: 'USER_ENTITY',
        useValue: UserSchema
    }
]

export const Entities = [{
        name: User.name,
        schema: UserSchema
    },
    {
        name: Task.name,
        schema: TaskSchema
    },
    {
        name: Project.name,
        schema: ProjectSchema
    }
]
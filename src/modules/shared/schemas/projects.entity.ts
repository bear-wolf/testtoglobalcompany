import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { SchemaTypes, Types, Document, HydratedDocument } from 'mongoose';

export type ProjectDocument = HydratedDocument<Project>;

export enum ProjectStatus {
    NEW = 0,
    In_Progress = 1,
    Done = 2
}

export const ProjectTableName = 'Projects'
@Schema()
export class Project {
    @Prop({ type: SchemaTypes.ObjectId})
    _id: string;

    @Prop()
    name: string;

    @Prop()
    description: string;

    @Prop({
        required: true,
        field: 'user_id'
    })
    userID: string;

    @Prop()
    picture: string;

    /*
       0 - new,
       1 - in_progress,
       2 - done
   * */
    @Prop({
        defaultValue: 0
    })
    status: number;

    @Prop({
        field: 'is_delete'
    })
    isDelete: boolean;

    @Prop({
        field: 'created_at'
    })
    createdAt: number;

    @Prop({
        defaultValue: null,
        field: 'updated_at'
    })
    updatedAt: number;
}

export const ProjectSchema = SchemaFactory.createForClass(Project);
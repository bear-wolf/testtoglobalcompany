import {Prop, Schema, SchemaFactory} from '@nestjs/mongoose';
import {HydratedDocument, SchemaTypes} from 'mongoose';

export type TaskDocument = HydratedDocument<Task>;

export enum TaskStatus {
    NEW = 0,
    In_Progress = 1,
    Done = 2
}

export const TaskTableName = 'Tasks'

@Schema()
export class Task {
    @Prop({type: SchemaTypes.ObjectId})
    _id: string;

    @Prop()
    name: string;

    @Prop()
    description: string;

    @Prop({
        required: true,
        field: 'user_id'
    })
    userID: string;

    @Prop({
        required: true,
        field: 'project_id'
    })
    projectID: string;

    /*
    0 - new,
    1 - in_progress,
    2 - done
    * */
    @Prop({
        defaultValue: 0
    })
    status: number;

    @Prop()
    picture: string;

    @Prop({
        field: 'is_delete'
    })
    isDelete: boolean;

    @Prop({
        field: 'created_at'
    })
    createdAt: number;

    @Prop({
        defaultValue: null,
        field: 'updated_at'
    })
    updatedAt: number;
}

export const TaskSchema = SchemaFactory.createForClass(Task);
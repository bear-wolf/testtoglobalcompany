import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { SchemaTypes, Types, Document, HydratedDocument } from 'mongoose';

export type UserDocument = HydratedDocument<User>;

export const UserTableName = 'Users'

@Schema()
export class User {
    @Prop({ type: SchemaTypes.ObjectId})
    _id: string;

    @Prop({
        field: 'first_name'
    })
    firstName: string;

    @Prop({
        field: 'last_name'
    })
    lastName: string;

    @Prop()
    username: string;

    @Prop()
    email: string;

    @Prop()
    password: string;

    @Prop()
    picture: string;

    @Prop({
        field: 'is_delete'
    })
    isDelete: boolean;

    @Prop({
        defaultValue: null,
        field: 'authorization_at',
    })
    authorizationAt: string;

    @Prop({
        field: 'created_at',
    })
    createdAt: number;

    @Prop({
        defaultValue: null,
        field: 'updated_at',
    })
    updatedAt: number;
}

export const UserSchema = SchemaFactory.createForClass(User);
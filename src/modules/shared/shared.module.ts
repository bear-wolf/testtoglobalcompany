import {MiddlewareConsumer, Module, NestModule} from '@nestjs/common';
import 'dotenv/config';
import {CacheModule} from '@nestjs/cache-manager';
import {Entities, EntityProviders} from './schemas/entity.providers';
import {utilities as nestWinstonModuleUtilities, WinstonModule} from 'nest-winston';
import * as winston from 'winston';
import {LoggerMiddleware} from '../../middlewares/logger.middleware';
import {AuthGuard} from "./guards/auth.guard";
import {JWTService} from "./jwt.service";
import {MongooseModule} from "@nestjs/mongoose";
import {JwtModule, JwtService} from "@nestjs/jwt";
import {EventEmitterModule} from "@nestjs/event-emitter";

const {
    MONGO_DB_HOST, MONGO_DB_NAME, MONGO_DB_USERNAME, MONGO_DB_PASSWORD,
    DB_AUTH_SOURCE, MONGO_DB_PORT, JWT_SECRET
} = process.env;
const mongoHost = MONGO_DB_USERNAME && MONGO_DB_PASSWORD ? `${MONGO_DB_USERNAME}:${MONGO_DB_PASSWORD}@` : MONGO_DB_HOST;

@Module({
    imports: [
        EventEmitterModule.forRoot(),
        JwtModule.register({
            global: true,
            secret: process.env.JWT_SECRET,
            signOptions: {expiresIn: '60s'},
        }),
        CacheModule.register(),
        WinstonModule.forRoot({
            transports: [
                new winston.transports.Console({
                    format: winston.format.combine(
                        winston.format.timestamp(),
                        winston.format.ms(),
                        nestWinstonModuleUtilities.format.nestLike('MyApp', {
                            colors: true,
                            prettyPrint: true,
                            processId: true,
                        }),
                    ),
                }),
                new winston.transports.File({
                    filename: 'logs/warn.log',
                    level: 'warn',
                }),
                new winston.transports.File({
                    filename: 'logs/debug.log',
                    level: 'debug',
                }),
                new winston.transports.File({
                    filename: 'logs/error.log',
                    level: 'error',
                }),
                // other transports...
            ]
        }),
        MongooseModule
            .forRoot(`mongodb://${mongoHost}:${MONGO_DB_PORT}/${MONGO_DB_NAME}`),
        MongooseModule.forFeature(Entities)
    ],
    controllers: [],
    providers: [
        AuthGuard,
        JwtService,
        JWTService,
        ...EntityProviders
    ],
    exports: [
        AuthGuard,
        JWTService,
        JwtModule,
        ...EntityProviders,
        WinstonModule,
        MongooseModule,
        CacheModule
    ],
})
export class SharedModule implements NestModule {
    configure(consumer: MiddlewareConsumer) {
        consumer
            .apply(LoggerMiddleware)
            .forRoutes('v1');
    }
}

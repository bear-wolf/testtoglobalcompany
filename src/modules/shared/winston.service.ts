import {Injectable} from "@nestjs/common";

export {}
import * as winston from 'winston'

// const levels = {
//     error: 0,
//     warn: 1,
//     info: 2,
//     http: 3,
//     verbose: 4,
//     debug: 5,
//     silly: 6
// };

@Injectable()
export class WinstonService {
    public logger = null;

    constructor() {
        this.logger = winston.createLogger({
            level: 'info',
            format: winston.format.json(),
            // defaultMeta: { service: 'user-service' },
            transports: [
                new winston.transports.Console(),
                new winston.transports.File({filename: 'logs/inform.log', level: 'info'}),
                new winston.transports.File({filename: 'logs/debug.log', level: 'debug'}),
                new winston.transports.File({filename: 'logs/error.log', level: 'error'}),
                new winston.transports.File({filename: 'logs/combined.log'})
            ]
        });
    }

}

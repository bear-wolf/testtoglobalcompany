import {IsOptional} from 'class-validator';
import {Prop} from "@nestjs/mongoose";
import {ApiPropertyOptional} from "@nestjs/swagger";

export class QueryDto {
    @IsOptional()
    @Prop()
    status: number

    @IsOptional()
    limit: number

    @ApiPropertyOptional({ description: 'Created date' })
    @IsOptional()
    @Prop()
    createdAt: 'desc' | 'asc'

    @ApiPropertyOptional({ description: 'Updated date' })
    @IsOptional()
    @Prop()
    updatedAt: 'desc' | 'asc'
}
import {IsNumber, IsOptional, IsString} from 'class-validator';
import {Exclude} from 'class-transformer';
import {Prop} from "@nestjs/mongoose";
import {SchemaTypes} from "mongoose";
import {ApiProperty, ApiPropertyOptional} from "@nestjs/swagger";

export class TaskDto {
  @Prop({ type: SchemaTypes.ObjectId})
  _id: string

  @ApiProperty({ description: 'Task name ' })
  @Prop()
  name: string;

  @ApiProperty({ description: 'Task description' })
  @Prop()
  description: string;

  @ApiProperty({ description: 'UserID' })
  @Prop({
    required: true,
    field: 'user_id'
  })
  userID: string;

  @ApiProperty({ description: 'ProjectID' })
  @Prop({
    required: true,
    field: 'project_id'
  })
  projectID: string;

  /*
  0 - new,
  1 - in_progress,
  2 - done
  * */
  @ApiProperty({ description: 'Status 0:new, 1:in_progress 2:done' })
  @Prop({
    defaultValue: 0
  })
  status: number;

  @ApiPropertyOptional({ description: 'Picture' })
  @Prop()
  picture: string;

  @ApiPropertyOptional({ description: 'Mark for delete' })
  @Prop({
    field: 'is_delete'
  })
  isDelete: boolean;

  @ApiPropertyOptional({ description: 'Date create' })
  @Prop({
    field: 'created_at'
  })
  createdAt: number;

  @ApiPropertyOptional({ description: 'Date update' })
  @Prop({
    defaultValue: null,
    field: 'updated_at'
  })
  updatedAt: number;
}
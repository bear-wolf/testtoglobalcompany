import {
    Body,
    Controller,
    Delete,
    Get,
    HttpException,
    HttpStatus,
    NotFoundException,
    Param,
    Post,
    Put, Query,
    Req,
    Res
} from '@nestjs/common';
import {TaskService} from "./task.service";
import {isIDInParamsDto} from "../../dtos/isID.dto";
import {Request, Response} from "express";
import {TaskDto} from "./dto/task.dto";
import {QueryDto} from "./dto/query.dto";
import {ApiOperation, ApiResponse} from "@nestjs/swagger";

//@UseGuards(AuthGuard)
@Controller('tasks')
export class TaskController {
    constructor(
        private readonly taskService: TaskService) {
    }

    @ApiOperation({ summary: 'Get lift of tasks' })
    @ApiResponse({ status: 200, description: 'Successfully' })
    @Get('list')
    async getList(@Query() query: QueryDto): Promise<any[]> {
        return await this.taskService.getList(query);
    }

    @ApiOperation({ summary: 'Create the task' })
    @ApiResponse({ status: 200, description: 'Successfully' })
    @Post()
    async create(@Body() schema: TaskDto, @Req() req: Request, @Res() res: Response): Promise<any> {
        let task;

        try {
            task = await this.taskService.create(schema);
        } catch (error) {
            throw new HttpException('Something wrong', HttpStatus.INTERNAL_SERVER_ERROR)
        }

        res.status(200).json(task);
    }

    @ApiOperation({ summary: 'Get the task by ID' })
    @ApiResponse({ status: 200, description: 'Successfully' })
    @ApiResponse({ status: HttpStatus.NOT_FOUND, description: 'Not found' })
    @Post(':_id')
    async getByID(@Param() body: isIDInParamsDto, @Res() res: Response): Promise<any> {
        const {_id} = body;
        const user = await this.taskService.getByID(_id);

        if (!user) throw new NotFoundException(`Task not found`);

        res.status(200).json(user || {});
    }

    @ApiOperation({ summary: 'Update the task by ID' })
    @ApiResponse({ status: 200, description: 'Successfully' })
    @ApiResponse({ status: HttpStatus.NOT_FOUND, description: 'Not found' })
    @Put(':_id')
    async update(@Param() body: isIDInParamsDto, @Body() schema: TaskDto, @Req() req: Request, @Res() res: Response): Promise<any> {
        const {_id} = body;
        const user = await this.taskService.update(_id, schema);

        res.status(200).json(user);
    }

    @ApiOperation({ summary: 'Remove the task by ID' })
    @ApiResponse({ status: 200, description: 'Successfully' })
    @Delete(':_id')
    async remove(@Param() body: isIDInParamsDto, @Res() res: Response): Promise<any> {
        const {_id} = body;
        await this.taskService.remove(_id);
        res.status(200).json({});
    }
}

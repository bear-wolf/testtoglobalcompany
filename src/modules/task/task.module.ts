import {Module} from '@nestjs/common';
import {TaskController} from "./task.controller";
import {TaskService} from "./task.service";
import {SharedModule} from "../shared/shared.module";

@Module({
    imports: [
        SharedModule
    ],
    controllers: [
        TaskController
    ],
    exports: [
        TaskService
    ],
    providers: [
        TaskService
    ]
})
export class TaskModule {
}

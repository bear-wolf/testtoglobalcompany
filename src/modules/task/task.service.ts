import {HttpException, HttpStatus, Injectable, NotFoundException} from '@nestjs/common';
import 'dotenv/config';
import {Task, TaskDocument, TaskStatus} from '../shared/schemas/tasks.entity';
import {InjectModel} from "@nestjs/mongoose";
import mongoose, {Model} from 'mongoose';
import {QueryDto} from "./dto/query.dto";

@Injectable()
export class TaskService {
    constructor(
        @InjectModel(Task.name) private task: Model<Task>) {
    }

    async getList(query?: QueryDto): Promise<TaskDocument[]> {
        const {status, limit, createdAt, updatedAt} = query;
        return await this.task.find({
            ...(status && {status}),
            //isDelete: false,
        }, {}, {
            ...(limit && {limit})
        })
            .sort({
                ...(createdAt && {createdAt}),
                ...(updatedAt && {updatedAt})
            })
    }

    async getByID(id: string): Promise<TaskDocument> {
        return await this.task.findById(id)
    }

    async create(body: any): Promise<any> {
        const object = {
            _id: new mongoose.Types.ObjectId(),
            status: TaskStatus.NEW,
            ...(body.name && {name: body.name}),
            ...(body.description && {description: body.description}),
            ...(body.userID && {userID: body.userID}),
            ...(body.projectID && {projectID: body.projectID}),
            isDelete: false,
            createdAt: Date.now()
        }
        return await this.task.create(object);
    }

    async update(ID: string, body: any): Promise<any> {
        const task: TaskDocument = await this.task.findById(ID);

        if (!task) throw new NotFoundException(`Task not found`);

        body.name && (task.name = body.name);
        body.description && (task.description = body.description);
        body.projectID && (task.projectID = body.projectID);
        body.userID && (task.userID = body.userID);
        body.updatedAt = Date.now();

        if (body.status) {
            if (![TaskStatus.NEW, TaskStatus.In_Progress, TaskStatus.Done].includes(body.status)) {
                throw new HttpException('Status param is not correct', HttpStatus.BAD_REQUEST)
            }
            task.status = body.status
        }

        await task.save();

        return task;
    }

    async remove(_id: string): Promise<any> {
        await this.task.deleteOne({_id});
        return {};
    }
}

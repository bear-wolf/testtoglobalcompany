import {Controller, Get, HttpStatus, Post, Req, Res} from '@nestjs/common';
import { EventEmitter2 } from '@nestjs/event-emitter';
import { Response, Request } from 'express';
import {ApiOperation, ApiResponse} from "@nestjs/swagger";

@Controller('test')
export class TestController {
  constructor(private eventEmitter: EventEmitter2) {
  }

  @ApiOperation({ summary: 'Check API work' })
  @ApiResponse({ status: 200, description: 'Successfully' })
  @Get('')
  async isTest(@Res() res: Response): Promise<any> {
    return res.status(200).send('API is Working');
  }

  @ApiOperation({ summary: 'Example use event emitter' })
  @ApiResponse({ status: 200, description: 'Successfully' })
  @Post()
  async test(@Req() request: Request): Promise<any> {
    this.eventEmitter.on('getBody', (p1) => {
      console.log('P1:', p1);
    })

    this.eventEmitter.emit('getBody', {p1: true});
    return {};
  }
}

import { Module } from '@nestjs/common';
import { SharedModule } from '../shared/shared.module';
import 'dotenv/config';
import { TestController } from './test.controller';

@Module({
  imports: [
    SharedModule,
  ],
  controllers: [
    TestController
  ],
  providers: [],
})
export class TestModule {
}

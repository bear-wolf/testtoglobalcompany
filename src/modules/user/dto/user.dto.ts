import {IsOptional} from 'class-validator';
import {Exclude} from 'class-transformer';
import {Prop} from "@nestjs/mongoose";
import {SchemaTypes} from "mongoose";
import {ApiOperation, ApiProperty, ApiPropertyOptional, ApiResponse} from "@nestjs/swagger";
import {HttpStatus} from "@nestjs/common";

export class UserDto {
    @Prop({type: SchemaTypes.ObjectId})
    _id: string

    @ApiProperty({ description: 'First name' })
    @Prop()
    firstName: string;

    @ApiProperty({ description: 'Last name' })
    @Prop()
    lastName: string;

    @ApiProperty({ description: 'Username' })
    @Prop()
    username: string;

    @ApiPropertyOptional({ description: 'Picture like base64 format' })
    @Prop()
    picture: string

    @Exclude({toPlainOnly: true})
    password: string;

    @Prop()
    createdAt: Date;

    @IsOptional()
    @Prop()
    updatedAt: Date;
}
import {
    Body,
    Controller,
    Delete,
    Get,
    HttpStatus,
    NotFoundException,
    Param,
    Post,
    Put,
    Query,
    Req,
    Res
} from '@nestjs/common';
import {UserService} from "./user.service";
import {isIDInParamsDto} from "../../dtos/isID.dto";
import {Request, Response} from "express";
import {UserDto} from "./dto/user.dto";
import {ApiOperation, ApiProperty, ApiResponse} from "@nestjs/swagger";
import {QueryDto} from "./dto/query.dto";

//@UseGuards(AuthGuard)
@Controller('users')
export class UserController {
    constructor(
        private readonly userService: UserService) {
    }

    @ApiOperation({ summary: 'Get list of users' })
    @ApiResponse({ status: 200, description: 'Successfully' })
    @Get('list')
    async getList(@Query() query: QueryDto): Promise<any> {
        return await this.userService.getList(query);
    }

    @ApiOperation({ summary: 'Get user by ID' })
    @ApiResponse({ status: 200, description: 'Successfully' })
    @ApiResponse({ status: HttpStatus.NOT_FOUND, description: 'Not found' })
    @Post(':_id')
    async getByID(@Param() body: isIDInParamsDto, @Res() res: Response): Promise<any> {
        const {_id} = body;
        const user = await this.userService.getByID(_id);

        if (!user) throw new NotFoundException(`User not found`);

        res.status(200).json(user || {});
    }

    @ApiOperation({ summary: 'Update user by ID' })
    @ApiResponse({ status: 200, description: 'Successfully' })
    @Put(':_id')
    async update(@Param() body: isIDInParamsDto, @Body() schema: UserDto, @Req() req: Request, @Res() res: Response): Promise<any> {
        const {_id} = body;

        const user = await this.userService.update(_id, schema);
        res.status(200).json(user);
    }

    @ApiOperation({ summary: 'Remove user by ID' })
    @ApiResponse({ status: 200, description: 'Successfully' })
    @Delete(':_id')
    async remove(@Param() body: isIDInParamsDto, @Res() res: Response): Promise<any> {
        const {_id} = body;
        await this.userService.remove(_id);
        res.status(200).json({});
    }
}

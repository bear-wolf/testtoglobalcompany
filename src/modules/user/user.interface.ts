export interface User {
    id?: number;
    lastName?: string;
    username?: string;
    email?: string;
    password?: string;
    picture?: string;
    isDelete?: boolean;
    authorizationAt?: Date;
    createdAt?: Date
    updatedAt?: Date
}
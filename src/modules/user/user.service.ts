import {Injectable} from '@nestjs/common';
import 'dotenv/config';
import {User, UserDocument} from '../shared/schemas/users.entity';
import {InjectModel} from "@nestjs/mongoose";
import {Model} from 'mongoose';
import * as moment from "moment/moment";
import {QueryDto} from "./dto/query.dto";

@Injectable()
export class UserService {
    constructor(
        @InjectModel(User.name) private user: Model<User>) {
    }

    async getList(query?: QueryDto): Promise<UserDocument[]> {
        const {limit, createdAt, updatedAt} = query;
        const list: UserDocument[] = await this.user.find({
            }, {}, {
                ...(limit && {limit})
            })
            .sort({
                ...(createdAt && {createdAt}),
                ...(updatedAt && {updatedAt})
            })

        return list;
    }

    async getByID(id: string): Promise<UserDocument> {
        return await this.user.findById(id)
    }

    async update(_id: string, body: any): Promise<any> {
        const user: UserDocument = await this.user.findById(_id);

        if (!user) return;

        body.firstName && (user.firstName = body.firstName);
        body.lastName && (user.lastName = body.lastName);
        body.username && (user.username = body.username);
        body.picture && (user.picture = body.picture);
        user.updatedAt = moment().unix();
        await user.save();

        return user
    }

    async remove(_id: string): Promise<any> {
        await this.user.deleteOne({_id});
        return {};
    }
}

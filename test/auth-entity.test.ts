import axios from 'axios';
import 'dotenv/config'

describe('Test auth entity', () => {
  const host = process.env.HOST;
  const port = process.env.PORT;

  const user = {
    firstName: "TestUser1",
    lastName: "TestUser1LastName",
    username: "user1",
    email: "user26@test.com",
    password: "14714711"
  }

  // it('Sign-UP', async () => {
  //   const response: any = await axios.post(`${host}:${port}/v1/auth/sign-up`, user)
  //   if (response.status == 400) {
  //     expect(1).toEqual(0);
  //     return;
  //   }
  //   const info = response.data;
  //   expect(info && 1).toEqual(1);
  // });

  it('Sign-In', async () => {
    const response = await axios.post(`${host}:${port}/v1/auth/sign-in`, {
      email: user.email,
      password: user.password
    })
    if (response.status == 400) {
      expect(1).toEqual(0);
      return;
    }

    const info = response.data;
    expect(info && 1).toEqual(1);
  });
});

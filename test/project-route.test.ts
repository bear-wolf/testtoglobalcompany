import axios from 'axios';
import 'dotenv/config'

describe('Test Project route to API', () => {
  const host = process.env.HOST;
  const port = process.env.PORT;

  it('Get project list', async () => {
    const response: any = await axios.get(`${host}:${port}/v1/projects/list`)
    if (response.status == 400) {
      expect(1).toEqual(0);
      return;
    }
    const list = response.data;
    expect(list.length).toEqual(list.length);
  });
});

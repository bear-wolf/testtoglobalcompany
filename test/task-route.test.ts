import axios from 'axios';
import 'dotenv/config'

describe('Test tasks route to API', () => {
  const host = process.env.HOST;
  const port = process.env.PORT;

  it('Get tasks list', async () => {
    const response: any = await axios.get(`${host}:${port}/v1/tasks/list`)
    if (response.status == 400) {
      expect(1).toEqual(0);
      return;
    }
    const list = response.data;
    expect(list.length).toEqual(list.length);
  });
});
